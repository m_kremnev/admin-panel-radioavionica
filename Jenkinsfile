pipeline {
    agent any
    options {
        timestamps()
    }
    environment {
        CI = 'true'
        REGISTRY = 'registry.netlinux.ru'
        IMAGE_TAG = sh(returnStdout: true, script: "echo '${env.BUILD_TAG}' | sed 's/%2F/-/g'").trim()
    }
    stages {
        stage("Init") {
            steps {
                sh "make init"
            }
        }
        stage("Valid") {
            steps {
                sh "make php-validate-schema"
            }
        }
        stage("Lint") {
            parallel {
                stage("Backend") {
                    steps {
                        sh "make php-lint"
                    }
                }
                stage("Frontend") {
                    steps {
                        sh "make frontend-lint"
                    }
                }
            }
        }
        stage("Analyze") {
            steps {
                sh "make analyze"
            }
        }
        stage("Test") {
            parallel {
                stage("Backend") {
                    steps {
                        sh "make php-test"
                    }
                }
                stage("Frontend") {
                    steps {
                        sh "make frontend-test"
                    }
                }
            }
        }
        stage("Down") {
            steps {
                sh "make docker-down-clear"
            }
        }
        stage("Build") {
            steps {
                sh 'make build'
            }
        }
        stage("Testing") {
            stages {
                stage("Build") {
                    steps {
                        sh "REGISTRY=$REGISTRY IMAGE_TAG=$IMAGE_TAG make testing-build"
                    }
                }
                stage("Init") {
                    steps {
                        sh "make testing-init"
                    }
                }
                stage("Down") {
                    steps {
                        sh "make testing-down-clear"
                    }
                }
            }
        }
        stage("Push") {
            when {
                branch "master"
            }
            steps {
                withCredentials([
                    usernamePassword(
                        credentialsId: 'REGISTRY_AUTH',
                        usernameValidate: 'USER',
                        passwordValidate: 'PASSWORD'
                    )
                ]) {
                    sh "docker login -u=$USER -p='$PASSWORD' $REGISTRY"
                }
                sh "make push"
            }
        }
        stage("Deploy") {
            when {
                branch "master"
            }
            steps {
                withCredentials([
                    string(credentialsId: 'PRODUCTION_HOST', variable: 'HOST'),
                    string(credentialsId: 'PRODUCTION_PORT', variable: 'PORT'),
                    string(credentialsId: 'RADIOAVIONICA_DB_PASS', variable: 'RADIOAVIONICA_DB_PASS'),
                    string(credentialsId: 'MAILER_HOST', variable: 'MAILER_HOST'),
                    string(credentialsId: 'MAILER_PORT', variable: 'MAILER_PORT'),
                    string(credentialsId: 'MAILER_USER', variable: 'MAILER_USER'),
                    string(credentialsId: 'MAILER_FROM', variable: 'MAILER_FROM'),
                    string(credentialsId: 'MAILER_PASS', variable: 'MAILER_PASS')
                ]) {
                    sshagent (credentials: ['PRODUCTION_AUTH']) {
                        sh "BUILD_NUMBER=${env.BUILD_NUMBER} make deploy"
                    }
                }
            }
        }
    }
    post {
        always {
            sh "make docker-down-clear || true"
            sh "make testing-down-clear || true"
        }
    }
}