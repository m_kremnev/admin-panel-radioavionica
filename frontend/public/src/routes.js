import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));

//My component
const TripDocument = React.lazy(() => import('./views/trip/document/TripDocument'));
const Lists = React.lazy(() => import('./views/military/units/lists/Lists'));
const Defects = React.lazy(() => import('./views/military/units/defects/Defects'));
const AcceptanceDocuments = React.lazy(() => import('./views/military/acceptance/documents/AcceptanceDocuments'));
const TripCalc = React.lazy(() => import('./views/calculation/trip-calc/TripCalc'));
const CreateDefects = React.lazy(() => import('./modules/createDefects/CreateDefects'));
const AddUnit = React.lazy(() => import('./views/military/add/unit/AddUnit'));

const routes = [
	{ path: '/', exact: true, name: 'Главная' },
	{ path: '/dashboard', name: 'Dashboard', component: Dashboard },
	{ path: '/users', exact: true, name: 'Users', component: Users },
	{ path: '/users/:id', exact: true, name: 'User Details', component: User },
	//My routes
	{
		path: '/trip/document',
		exact: true,
		name: 'Отчетные документы',
		component: TripDocument,
	},
	{
		path: '/military/lists',
		exact: true,
		name: 'Список частей',
		component: Lists,
	},
	{
		path: '/military/defects',
		exact: true,
		name: 'Неисправности',
		component: Defects,
	},
	{
		path: '/military/acceptance/documents',
		exact: true,
		name: 'Документы для военной приемки',
		component: AcceptanceDocuments,
	},
	{
		path: '/calculation/trip-calc',
		exact: true,
		name: 'Расчет командировки',
		component: TripCalc,
	},
	{
		path: '/add/defect',
		exact: true,
		name: 'Добавление дефекта',
		component: CreateDefects,
	},
	{
		path: '/add/unit',
		exact: true,
		name: 'Добавление воинской части',
		component: AddUnit,
	},
];

export default routes;
