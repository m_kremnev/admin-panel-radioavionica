import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { AppContainer } from './AppContainer';
import { Provider } from 'react-redux';
import { store } from './store';

import { icons } from './assets/icons';

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
React.icons = icons;

export const App: React.FC<{}> = () => {
	return (
		<Provider store={store}>
			<Router>
				<AppContainer />
			</Router>
		</Provider>
	);
};
