module.exports = {
	presets: [
		'@babel/preset-env',
		'@babel/preset-react',
		'@babel/preset-typescript',
		[
			'@emotion/babel-preset-css-prop',
			{
				options: {
					autoLabel: true,
					labelFormat: '[local]',
					useBuiltIns: false,
					throwIfNamespace: true,
				},
			},
		],
	],
	plugins: [['@babel/plugin-proposal-class-properties'], ['@babel/plugin-transform-runtime'], '@babel/plugin-transform-react-jsx'],
	env: {
		test: {
			plugins: ['@babel/plugin-transform-modules-commonjs'],
		},
		production: {
			plugins: ['emotion'],
		},
		development: {
			plugins: [['emotion', { sourceMap: true }]],
		},
	},
};
